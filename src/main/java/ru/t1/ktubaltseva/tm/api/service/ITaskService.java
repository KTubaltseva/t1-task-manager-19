package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name, String description) throws DescriptionEmptyException, NameEmptyException;

    Task create(String name) throws NameEmptyException;

    Task changeTaskStatusById(
            String id,
            Status status
    ) throws TaskNotFoundException, IdEmptyException;

    Task changeTaskStatusByIndex(
            Integer index,
            Status status
    ) throws IndexIncorrectException, TaskNotFoundException;

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(
            String id,
            String name,
            String description
    ) throws AbstractFieldException, TaskNotFoundException;

    Task updateByIndex(
            Integer index,
            String name,
            String description
    ) throws TaskNotFoundException, AbstractFieldException;

}
