package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description) throws AbstractFieldException;

    Project create(String name) throws NameEmptyException;

    Project changeProjectStatusById(
            String id,
            Status status
    ) throws IdEmptyException, ProjectNotFoundException;

    Project changeProjectStatusByIndex(
            Integer index,
            Status status
    ) throws IndexIncorrectException, ProjectNotFoundException;

    List<Project> findAll(Sort sort);

    Project updateById(
            String id,
            String name,
            String description
    ) throws AbstractFieldException, ProjectNotFoundException;

    Project updateByIndex(
            Integer index,
            String name,
            String description
    ) throws AbstractFieldException, ProjectNotFoundException;

}
