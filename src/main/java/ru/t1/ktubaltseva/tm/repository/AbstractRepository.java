package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> {

    protected final List<M> models = new ArrayList<>();

    public M add(final M model) {
        models.add(model);
        return model;
    }

    public void clear() {
        models.clear();
    }

    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    public List<M> findAll() {
        return models;
    }

    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    public M findOneById(final String id) {
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    public int getSize() {
        return models.size();
    }

    public M remove(final M model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }
}
