package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        return add(project);
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project(name);
        return add(project);
    }

}
