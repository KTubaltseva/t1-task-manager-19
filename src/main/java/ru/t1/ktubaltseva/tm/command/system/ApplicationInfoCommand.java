package ru.t1.ktubaltseva.tm.command.system;

import ru.t1.ktubaltseva.tm.util.FormatUtil;

public final class ApplicationInfoCommand extends AbstractSystemCommand {

    private final String NAME = "info";

    private final String ARGUMENT = "-i";

    private final String DESC = "Display system info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();

        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.formatBytes(maxMemory));
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.printf("Available processors (cores):\t %s\n", availableProcessors);
        System.out.println();
        System.out.printf("Free memory:\t %s\n", freeMemoryFormat);
        System.out.printf("Maximum memory:\t %s\n", maxMemoryFormat);
        System.out.printf("Total memory:\t %s\n", totalMemoryFormat);
        System.out.printf("Usage memory:\t %s\n", usageMemoryFormat);
        System.out.println();
    }

}
