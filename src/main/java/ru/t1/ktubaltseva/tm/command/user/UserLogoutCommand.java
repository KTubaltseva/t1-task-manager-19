package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "user-logout";

    private final String DESC = "Logout current user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOGOUT USER]");
        getAuthService().logout();
    }

}
