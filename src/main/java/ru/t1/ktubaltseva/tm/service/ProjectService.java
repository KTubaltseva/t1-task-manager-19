package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IProjectService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository modelRepository) {
        super(modelRepository);
    }

    @Override
    public Project changeProjectStatusById(
            final String id,
            final Status status
    ) throws IdEmptyException, ProjectNotFoundException {
        Project project;
        try {
            project = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(
            final Integer index,
            final Status status
    ) throws IndexIncorrectException, ProjectNotFoundException {
        Project project;
        try {
            project = findOneByIndex(index);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return modelRepository.create(name, description);
    }

    @Override
    public Project create(final String name) throws NameEmptyException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return modelRepository.create(name);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return modelRepository.findAll(sort.getComparator());
    }

    @Override
    public Project updateById(
            final String id,
            final String name,
            final String description
    ) throws AbstractFieldException, ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Project project;
        try {
            project = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(
            final Integer index,
            final String name,
            final String description
    ) throws AbstractFieldException, ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Project project;
        try {
            project = findOneByIndex(index);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
