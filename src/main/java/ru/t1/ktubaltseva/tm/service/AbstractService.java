package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IRepository;
import ru.t1.ktubaltseva.tm.api.service.IService;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R modelRepository;

    public AbstractService(final R modelRepository) {
        this.modelRepository = modelRepository;
    }

    public M add(final M model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        modelRepository.add(model);
        return model;
    }

    public void clear() {
        modelRepository.clear();
    }

    public List<M> findAll() {
        return modelRepository.findAll();
    }

    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return modelRepository.findAll(comparator);
    }

    public M findOneById(final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = modelRepository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    public M findOneByIndex(final Integer index) throws IndexIncorrectException, EntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize()) throw new IndexIncorrectException();
        final M model = modelRepository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    public M remove(final M model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        modelRepository.remove(model);
        return model;
    }

    public M removeById(final String id) throws IdEmptyException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return modelRepository.removeById(id);
    }

    public M removeByIndex(final Integer index) throws IndexIncorrectException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize()) throw new IndexIncorrectException();
        return modelRepository.removeByIndex(index);
    }

}
