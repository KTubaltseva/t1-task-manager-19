package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository modelRepository) {
        super(modelRepository);
    }

    @Override
    public Task create(final String name, final String description) throws NameEmptyException, DescriptionEmptyException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return modelRepository.create(name, description);
    }

    @Override
    public Task create(final String name) throws NameEmptyException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return modelRepository.create(name);
    }

    @Override
    public Task changeTaskStatusById(
            final String id,
            final Status status
    ) throws TaskNotFoundException, IdEmptyException {
        Task task;
        try {
            task = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(
            final Integer index,
            final Status status
    ) throws IndexIncorrectException, TaskNotFoundException {
        Task task;
        try {
            task = findOneByIndex(index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return modelRepository.findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return modelRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task updateById(
            final String id,
            final String name,
            final String description
    ) throws AbstractFieldException, TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task;
        try {
            task = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(
            final Integer index,
            final String name,
            final String description
    ) throws AbstractFieldException, TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task;
        try {
            task = findOneByIndex(index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
