package ru.t1.ktubaltseva.tm.component;

import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.command.project.*;
import ru.t1.ktubaltseva.tm.command.system.*;
import ru.t1.ktubaltseva.tm.command.task.*;
import ru.t1.ktubaltseva.tm.command.user.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.UserRepository;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectDisplayByIdCommand());
        registry(new ProjectDisplayByIndexCommand());
        registry(new ProjectDisplayListCommand());
        registry(new ProjectDisplayListFullInfoCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskDisplayByIdCommand());
        registry(new TaskDisplayByIndexCommand());
        registry(new TaskDisplayByProjectIdCommand());
        registry(new TaskDisplayListCommand());
        registry(new TaskDisplayListFullInfoCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserDisplayProfileCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() throws AbstractException {
        projectService.add(new Project("pn2", "pd2", Status.IN_PROGRESS));
        projectService.add(new Project("pn5", "pd5", Status.NOT_STARTED));
        projectService.add(new Project("pn3", "pd3", Status.IN_PROGRESS));
        projectService.add(new Project("pn4", "pd4", Status.COMPLETED));

        taskService.add(new Task("tn2", "td2", Status.IN_PROGRESS));
        taskService.add(new Task("tn5", "td5", Status.NOT_STARTED));
        taskService.add(new Task("tn3", "td3", Status.IN_PROGRESS));
        taskService.add(new Task("tn4", "td4", Status.COMPLETED));

        userService.create("USER1", "USER1");
        userService.create("USER2", "USER2");
        userService.create("ADMIN", "ADMIN", Role.ADMIN);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private boolean processArgument(final String[] args) throws ArgumentNotSupportedException {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String argument) throws ArgumentNotSupportedException {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
    }

    private void processCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void run(final String[] args) throws AbstractException {
        if (processArgument(args)) System.exit(0);

        initDemoData();
        initLogger();

        processCommands();
    }

}
